//
//  ImageModel.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 12/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "ImageModel.h"
#import <Photos/Photos.h>

NSString *const ImagesFilename = @"ImageModelItems";

@interface ImageModel () <PHPhotoLibraryChangeObserver>

@property (nonatomic, strong) NSMutableArray *mutableImages;
@property (nonatomic, strong) PHFetchResult *fetchResult;

@end

@implementation ImageModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
        
        _fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
        
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        _mutableImages = [NSKeyedUnarchiver unarchiveObjectWithFile:[path stringByAppendingPathComponent:ImagesFilename]];
        if (!_mutableImages) {
            _mutableImages = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (void)photoLibraryDidChange:(PHChange *)changeInstance {
    PHFetchResultChangeDetails *details = [changeInstance changeDetailsForFetchResult:self.fetchResult];

    if (details) {
        self.fetchResult = details.fetchResultAfterChanges;
    }
}

- (NSInteger)sectionsCount {
    return self.mutableImages.count > 0 ? 2 : 1;
}

- (NSInteger)countForSection:(NSInteger)section {
    switch (section) {
        case 0:
            return self.mutableImages.count;
        case 1:
            return self.fetchResult.count;
    }
    return 0;
}

- (id)itemForIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0:
            return self.mutableImages[indexPath.item];
        case 1:
            return self.fetchResult[indexPath.item];
    }
    return nil;
}

- (void)saveImage:(UIImage *)image {
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageName = [NSUUID UUID].UUIDString;
    
    [UIImagePNGRepresentation(image) writeToFile:[path stringByAppendingPathComponent:imageName]
                                      atomically:YES];
    [self.mutableImages addObject:imageName];
    
    [NSKeyedArchiver archiveRootObject:self.mutableImages toFile:[path stringByAppendingPathComponent:@"ImagesFilename"]];
}

@end
