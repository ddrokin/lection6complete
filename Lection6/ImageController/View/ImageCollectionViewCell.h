//
//  ImageCollectionViewCell.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 12/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;

@end
